import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import MenuItems from "../menuComponent/menu";

class ButtonMenu extends Component {
  state = {
    id: this.props.id,
    issues: this.props.issues,
    currentIssues: this.props.issues,
    sort: null,
    sortBy: this.props.sortBy,
    data: this.props.data,
    avatars: this.props.avatars,
    updateOnce: false
  };

  componentDidUpdate() {
    if (this.props.data.length > 0 && !this.state.updateOnce) {
      this.setState({
        data: this.props.data,
        avatars: this.props.avatars,
        updateOnce: true
      });
    }
  }

  handleClick = event => {
    if (event)
      this.setState({
        sort: event.currentTarget
      });

    return this.state.sort;
  };

  sendSort = sort => {
    this.setState({
      sort
    });
  };

  render() {

    return (
      <span>
        <Button
          aria-owns={this.state.sort ? "simple-menu" : undefined}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <span>{this.state.id}</span>
          <Icon>keyboard_arrow_down</Icon>
        </Button>
        <MenuItems
          id={this.state.id}
          issues={this.props.issues}
          data={this.state.data}
          avatars={this.props.avatars}
          getSort={this.sendSort}
          addLabel={this.props.addLabel}
          sort={this.state.sort}
          sortBy={this.props.sortBy}
          filterBy={this.props.filterBy}
        />
      </span>
    );
  }
}

export default ButtonMenu;
