import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles, TextField, Button, Typography } from "@material-ui/core";
import { Avatar, Card, CardHeader, CardContent } from "@material-ui/core";
import moment from "moment";

const styles = theme => ({
  card: {
    marginLeft: "10%",
    marginRight: "10%"
  },
  form: {
    marginLeft: "10%",
    marginRight: "10%"
  }
});

class IssueComment extends Component {
  state = {
    issueComments: [],
    issueComment: {},
    newComment: "",
    created_at: Date,
    editFlag: false,
    buttonText: "Close"
  };

  getGithubIssuesComments = () => {
    const { id } = this.props;
    fetch(
      `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/${id}/comments`,
      {
        headers: {
          Authorization: "token 188f43da499510f74e2292806445de7cff58f3be"
        }
      }
    )
      .then(val => val.json())
      .then(data =>
        this.setState(
          {
            issueComments: data
          }
        )
      );
  };

  componentDidMount() {
    this.getGithubIssuesComments();
  }

  handleAddComment = e => {
    let text = e.target.value;
    this.setState({
      newComment: text
    });
  };

  editComment = (e, issueComment) => {
    this.setState({
      issueComment,
      editFlag: true
    });
  };

  deleteComment = (e, issueComment) => {
    const { issueComments } = this.state;
    if (issueComments.includes(issueComment)) {
      const index = issueComments.indexOf(issueComment);
      issueComments.splice(index, 1);
      this.setState({
        issueComments
      });
    }
  };

  closeIssue = e => {
    const { buttonText } = this.state;
    const closeButton = e.target;
    const val = closeButton.textContent === "Close" ? "Close" : "Open";
    this.props.closeIssue(val);
    this.setState({
      buttonText: buttonText === "Close" ? "Reopen" : "Close"
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { editFlag, issueComments, issueComment } = this.state;
    if (!editFlag) {
      let newComment = issueComments.concat({
        user: { login: "Omkar" },
        body: this.state.newComment,
        created_at: new Date()
      });
      this.setState({
        issueComments: newComment,
        newComment: ""
      });
    } else {
      if (issueComments.includes(issueComment)) {
        issueComment.body = this.state.newComment;
        this.setState({
          issueComments,
          newComment: "",
          editFlag: false
        });
      }
    }
  };
  getAvatar = issueComment => {
    if (issueComment.user) {
      return <img src={issueComment.user.avatar_url} />;
    }
  };

  getUser = issueComment => {
    if (issueComment.user) {
      return <span>{issueComment.user.login}</span>;
    }
  };
  renderComments = (issueComments, classes) => {

    if (issueComments.length > 0) {
      return issueComments.map((issueComment, i) => {
        return (
          <Card className={classes.card}>
            <CardHeader
              avatar={
                <Avatar aria-label="Recipe" className={classes.avatar}>
                  {this.getAvatar(issueComment)}
                </Avatar>
              }
              action={
                <div>
                  <Button
                    onClick={e => {
                      this.editComment(e, issueComment);
                    }}
                  >
                    Edit
                  </Button>
                  <Button
                    onClick={e => {
                      this.deleteComment(e, issueComment);
                    }}
                  >
                    Delete
                  </Button>
                </div>
              }
              title={this.getUser(issueComment)}
              subheader={moment(issueComment.created_at)
                .startOf("day")
                .fromNow()}
            />
            <CardContent>
              <Typography component="p">{issueComment.body}</Typography>
            </CardContent>
          </Card>
        );
      });
    }
  };
  render() {
    const { classes } = this.props;
    const { issueComments, buttonText } = this.state;
    return (
      <div className={classes.root}>
        {this.renderComments(issueComments, classes)}
        <div className={classes.form}>
          <form onSubmit={this.handleSubmit}>
            <TextField
              onChange={this.handleAddComment}
              id="standard-full-width"
              style={{ margin: 8 }}
              placeholder="Add Comment"
              fullWidth
              margin="normal"
              value={this.state.newComment}
              InputLabelProps={{
                shrink: true
              }}
            />
            <label htmlFor="standard-full-width">
              <Button className="submit-btn" label="Submit" type="submit">
                Submit
              </Button>
              <Button data-key="1" onClick={this.closeIssue}>
                {buttonText}
              </Button>
            </label>
          </form>
        </div>
      </div>
    );
  }
}

IssueComment.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(IssueComment);
