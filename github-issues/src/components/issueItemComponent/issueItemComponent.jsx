import React, { Component } from "react";
import { Divider, Icon, Button, Chip, Paper } from "@material-ui/core";
import PropTypes from "prop-types";
import Axios from "axios";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";

import IssueComment from "./issueComment";
import ButtonMenu from "../buttonComponent/buttonMenu";

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit / 2
  },
  content: {
    marginLeft: "10%"
  },
  textView: {
    cursor: "pointer"
  },
  issueNumber: {
    color: "grey"
  },
  state: {
    display: "inline-block",
    borderRadius: "20%",
    height: "auto",
    color: "white",
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  icon: {
    fontSize: 14
  }
});

class IssueItemComponent extends Component {
  state = {
    issue: [],
    states: "Open",
    color: "green",
    labels: [],
    seedLabels: []
  };

  componentDidMount() {
    this.getGithubIssue();
    this.getGithubIssues();
  }

  getGithubIssue = () => {
    const BASE_URL =
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/";
    fetch(`${BASE_URL}${this.props.match.params.id}`, {
      headers: {
        Authorization: "token 188f43da499510f74e2292806445de7cff58f3be"
      }
    })
      .then(val => val.json())
      .then(data =>
        this.setState({
          issue: data,
          labels: data.labels
        })
      );
  };

  getGithubIssues = async () => {
    const BASE_URL =
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues";
    let issues = await Axios.get(`${BASE_URL}`, {
      headers: {
        Authorization: "token 188f43da499510f74e2292806445de7cff58f3be",
        Accept: "application/vnd.github.symmetra-preview+json"
      }
    });
    this.fetchLabels(issues.data);
  };
  fetchLabels = issues => {
    let labels = [];
    issues.forEach(issue => {
      if (issue.labels.length > 0) {
        labels.push(...issue.labels.map(label => label.name));
      }
    });
    labels = labels.filter((label, i) => labels.indexOf(label) === i);

    this.filterLabels(labels);
  };

  filterLabels = seedLabels => {
    const { labels } = this.state;

    const labelNames = labels.map(label => label.name);
    seedLabels.forEach(label => {
      if (labelNames.includes(label)) {
        const index = seedLabels.indexOf(label);
        seedLabels.splice(index, 1);
      }
    });
    this.setState({
      seedLabels
    });
  };
  getLabels = () => {
    const { labels } = this.state;

    if (labels) {
      return labels.map((label, i) => {
        return (
          <Chip
            key={i}
            label={label.name}
            style={{ background: "#" + label.color }}
            onDelete={e => {
              this.handleDelete(label);
            }}
          />
        );
      });
    }
  };

  addLabel = labelName => {
    const { labels, seedLabels } = this.state;
    const labelsCopy = labels.concat({ name: labelName });

    const seedLabelsCopy = seedLabels;
    if (seedLabelsCopy.includes(labelName)) {
      seedLabelsCopy.splice(seedLabelsCopy.indexOf(labelName), 1);
    }
    console.log(seedLabelsCopy);

    this.setState(
      {
        labels: labelsCopy,
        seedLabels: seedLabelsCopy
      },
      () => {
        console.log(seedLabels);
      }
    );
  };

  handleDelete = label => {
    const { labels, seedLabels } = this.state;

    const seedLabelsCopy = seedLabels;

    if (labels.includes(label)) {
      const index = labels.indexOf(label);
      let deletedLabel = labels.splice(index, 1);
      seedLabelsCopy.push(deletedLabel[0].name);
    }

    this.setState({
      labels,
      seedLabels: seedLabelsCopy
    });
  };

  closeIssue = states => {
    this.setState({
      states,
      color: states === "Open" ? "green" : "red"
    });
  };

  getAuthor = issue => {
    if (issue.user) {
      return (
        <span style={{ fontSize: "0.5em" }}>
          {" "}
          {issue.user.login}
          <span style={{ color: "grey" }}>
            {" "}
            opened this issue{" "}
            {moment(issue.createdAt)
              .startOf("day")
              .fromNow()}{" "}
            {issue.comments} comments
          </span>
        </span>
      );
    }
  };
  render() {
    const { issue, states, color } = this.state;
    const { classes, labels } = this.props;

    // console.log(labels);

    return (
      <div className={classes.root}>
        <div className={classes.content}>
          <Typography
            className={classes.textView}
            component="h2"
            variant="h4"
            gutterBottom
          >
            <div>
              <span>{issue.title}</span>
              <span className={classes.issueNumber}>#{issue.number}</span>
            </div>
            <div>
              <Button className={classes.open} style={{ background: color }}>
                <Icon className={classes.icon}>info_outlined</Icon>
                <span>{states}</span>
              </Button>

              {this.getAuthor(issue)}
              <ButtonMenu
                style={{ display: "inline-block", marginLeft: "1%" }}
                id="Add Labels"
                addLabel={this.addLabel}
                data={this.state.seedLabels}
              />
            </div>
          </Typography>
        </div>
        <Divider />
        <Paper className={classes.root} style={{ margin: "10px 10% 10px 10%" }}>
          {this.getLabels(issue)}
        </Paper>
        <IssueComment
          id={this.props.match.params.id}
          closeIssue={this.closeIssue}
        />
      </div>
    );
  }
}

IssueItemComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(IssueItemComponent);
