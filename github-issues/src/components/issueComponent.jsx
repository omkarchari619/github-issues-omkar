import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import { TextField, Chip } from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import * as moment from "moment";
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";

import ButtonMenu from "./buttonComponent/buttonMenu";
import Pages from "../components/pageComponent/pagination";

const styles = theme => ({
  header: {
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "rgb(128,128,255,1)",
    padding: "2%"
  },
  container: {
    display: "flex"
  },

  issueDetail: {
    height: "100vh"
  },
  list: {
    background: "white"
  },
  buttonDiv: {
    display: "flex"
  },
  titleSearch: {
    display: "flex",
    justifyContent: "space-between"
  },
  textField: {
    height: "40px",
    flex: "5"
  },
  label: {
    bottom: "-20px"
  },
  labelName: {
    marginRight: 20,
    marginLeft: 10,
    borderRadius: "50%"
  }
});

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

class Issues extends Component {
  state = {
    sort: null,
    issues: this.props.issues,
    currentIssues: this.props.issues,
    data: ["oldest", "newest", "updated", "least updated"],
    states: ["open", "closed"],
    issue: {},
    authors: this.props.authors,
    labels: this.props.labels,
    sortBy: this.props.sortBy,
    handleStateChange: this.props.handlePageChange,
    updateOnce: false,
  };

  componentDidUpdate() {
    if (
      (this.props.authors.length > 0 || this.props.labels.length > 0) &&
      !this.state.updateOnce
    ) {
      this.setState({
        authors: this.props.authors,
        labels: this.props.labels,
        updateOnce: true
      });
    }
  }

  getLabels = (classes, issue) => {
    return issue.labels.map((label, id) => {
      return (
        <span className={classes.labelName}>
          <Chip
            style={{ background: "#" + label.color }}
            label={label.name}
            variant="outlined"
          />
        </span>
      );
    });
  };


  handleIssueDetails = (issue, id) => {
    this.setState({
      issue,
      titleId: id
    });
  };

  handleTitleChange = (titleChange, titleId, title) => {
    this.setState({
      titleChange,
      titleId,
      title
    });
  };


  renderIssues = classes => {
    const { issues } = this.props;
    return issues.map((issue, id) => {
      return (
        <div key={id} onClick={e => this.handleIssueDetails(issue, id)}>
            <ListItemLink className={classes.list}>
              <Icon style={{ fontSize: "18px", color: "green" }}>
                info_outlined
              </Icon>
              <ListItemText
                primary={
                  <div style={{ height: "fit-content" }}>
                    <Link to={`/issues/${issue.number}`}>
                      {issue.title}
                    </Link>
                    {this.getLabels(classes, issue)}
                  </div>
                }
                secondary={`created at: ${moment(issue.created_at).format(
                  "MMMM Do YYYY, h:mm:ss a"
                )} updated at: ${moment(issue.updated_at).format(
                  "MMMM Do YYYY, h:mm:ss a"
                )}`}
              />
            </ListItemLink>
          <Divider />
        </div>
      );
    });
  };

  updateSearch = e => {
    this.props.callbackFromParent(e.target.value.substr(0, 20));
  };
  render() {
    const { classes} = this.props;

    return (
      <div className={classes.container}>
        <div className={classes.issueList}>
          <List className={classes.header} component="nav">
            <div className={classes.titleSearch}>
              <ButtonMenu
                id="Issues"
                issues={this.props.issues}
                data={this.state.states}
                sortBy={this.props.sortBy}
                filterBy={this.props.filterBy}
              />
            </div>
            <div className={classes.buttonDiv}>
              <TextField
                onChange={this.updateSearch}
                className={classes.textField}
                label="Search"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  FormLabelClasses: {
                    root: classes.label
                  }
                }}
              />
              <ButtonMenu
                id="Authors"
                issues={this.props.issues}
                data={this.state.authors}
                avatars={this.props.avatars}
                sortBy={this.props.sortBy}
                filterBy={this.props.filterBy}
              />
              <ButtonMenu
                id="Labels"
                issues={this.props.issues}
                data={this.state.labels}
                sortBy={this.props.sortBy}
                filterBy={this.props.filterBy}
              />
              <ButtonMenu
                id="Sort By"
                issues={this.props.issues}
                data={this.state.data}
                sortBy={this.props.sortBy}
              />
            </div>
          </List>
          <List component="nav"> {this.renderIssues(classes)} </List>
          <List className={classes.header} component="nav">
            <Pages handlePageChange={this.props.handlePageChange} />
          </List>
        </div>
      </div>
    );
  }
}

Issues.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Issues);
