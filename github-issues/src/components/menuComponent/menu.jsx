import React, { Component } from "react";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

class MenuItems extends Component {
  state = {
    id: this.props.id,
    data: this.props.data,
    avatars: this.props.avatars,
    sort: this.props.sort,
    sortBy: this.props.sortBy,
    filterBy: this.props.filterBy,
    sortChange: false,
    updateOnce: false
  };

  componentDidUpdate() {
    if (this.props.data.length > 0 && !this.state.updateOnce) {
      this.setState({
        issues: this.props.issues,
        data: this.props.data,
        avatars: this.props.avatars,
        updateOnce: true
      });
    }
  }

  changeState = name => {
    switch (name) {
      case "Authors":
        this.setState({
          sortChange: false,
          filterByAuthors: true,
          filterByLabels: false,
          filterByOpenOrClose: false
        });
        return this.state.filterByAuthors;
      case "Labels":
        this.setState({
          sortChange: false,
          filterByAuthors: false,
          filterByLabels: true,
          filterByOpenOrClose: false
        });
        return this.state.filterByLabels;
      case "Issues":
        this.setState({
          sortChange: false,
          filterByAuthors: false,
          filterByLabels: false,
          filterByOpenOrClose: true
        });
        return this.state.filterByOpenOrClose;
      default:
        this.setState({
          sortChange: true,
          filterByAuthors: false,
          filterByLabels: false,
          filterByOpenOrClose: false
        });
        return this.state.sortChange;
    }
  };
  handler = e => {
    this.props.getSort(this.state.sort);
    const {data} = this.state;
    
    if (e.target.id === "") {
      return;
    }
    if(this.state.id === 'Add Labels'){
      
      this.props.addLabel(e.target.textContent)
      return;
    }
    if(this.state.id === 'Authors'
    ||this.state.id === 'Labels'
    ||this.state.id === 'Issues'){
      this.state.filterBy(this.props.issues,this.state.id,e.target.textContent)
      return;
    }

    this.state.sortBy(this.props.issues, e.target.id);
  };

  renderAvatars = (avatar, id) => {
    return avatar === undefined ? (
      <div key={id} />
    ) : (
      <ListItemIcon key={id}>
        <img
          src={this.state.avatars[id]}
          style={{ borderRadius: "50%", width: "30px", height: "30px" }}
        />
      </ListItemIcon>
    );
  };

  renderMenuItems = () => {
    return this.state.data.map((menuItem, id) => (
      <div key={id}>
        <MenuItem key={id} id={id} onClick={this.handler}>
          {this.renderAvatars(this.state.avatars, id)}
          {menuItem}
        </MenuItem>
      </div>
    ));
  };
  render() {
    return (
      <Menu
        id="simple-menu"
        anchorEl={this.props.sort}
        open={Boolean(this.props.sort)}
        onClose={this.handler}
      >
        {this.renderMenuItems()}
      </Menu>
    );
  }
}

export default MenuItems;
