import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import App from "../../App";
import IssuesItemComponent from "../issueItemComponent/issueItemComponent";
class Routing extends Component {
  state = {
    labels:[]
  };


  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/issues" exact={true} component={App} />
          <Route
            path="/issues/:id" component={IssuesItemComponent}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Routing;
