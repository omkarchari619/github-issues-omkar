import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { List, Button } from "@material-ui/core";

const styles = theme => ({
  header: {
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "rgb(128,128,255,1)",
    padding: "2%"
  },
})
class Pages extends Component {
  state = {
    handlePageChange:this.props.handlePageChange
  }
  handlePagination= e => {
    this.state.handlePageChange(e.target.textContent)
  }

  render() {
    const {classes} = this.props
    return (
      <List className = {classes.header}>
        <div>
          <Button onClick={this.handlePagination}>Previous</Button>
          <Button onClick={this.handlePagination}>Next</Button>
        </div>
      </List>
    );
  }
}

Pages.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Pages);
