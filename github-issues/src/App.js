import React, { Component } from "react";
import "./App.css";
import DateDiff from "date-diff";
import Axios from "axios";

import issues from "./issues";
import Issues from "./components/issueComponent";

class App extends Component {
  state = {
    newIssues: [],
    seedIssues: [],
    currentIssues: [],
    authorIssues: [],
    labelIssues: [],
    stateIssues: [],
    authorFlag: 0,
    labelFlag: 0,
    stateFlag: 0,
    activePage: 1,
  };
  getGithubIssues = async activePage => {
    const BASE_URL =
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues";
    let issuesSeedData = await Axios.get(`${BASE_URL}?page=${activePage}`, {
      headers: {
        Authorization: "token 188f43da499510f74e2292806445de7cff58f3be",
        Accept: "application/vnd.github.symmetra-preview+json"
      }
    });
    this.setState({
      newIssues: [...issuesSeedData.data],
      seedIssues: [...issuesSeedData.data],
      currentIssues: [...issuesSeedData.data],
      authorIssues: [...issuesSeedData.data],
      labelIssues: [...issuesSeedData.data],
      stateIssues: [...issuesSeedData.data]
    });
  };

  componentDidMount() {
    this.getGithubIssues(this.state.activePage);
  }

  getAuthors = () => {
    let authors = this.state.newIssues
      .map(issue => issue.user.login)
      .filter((user, i, arr) => arr.indexOf(user) === i);
    return authors;
  };

  getAvatars = () => {
    return this.state.newIssues
      .map(issue => issue.user.avatar_url)
      .filter((user, i, arr) => arr.indexOf(user) === i);
  };

  getLabels = () => {
    let labels = [ ...new Set (
      this.state.newIssues.reduce ((obj, val) => {
        val.labels.filter (value => obj.push (value.name));
        return obj;
      }, [])
    )];
    return labels;
  };
  sortListItems = (issues, sortBy) => {
    this.setState({
      newIssues: issues.sort((issue1, issue2) => {
        return this.sortByOption(sortBy, issue1, issue2);
      })
    });
    return issues;
  };

  sortByOption(sortBy, issue1, issue2) {
    let dateDiff, date1, date2;

    switch (sortBy) {
      case "0":
        date1 = new Date(issue1.created_at);
        date2 = new Date(issue2.created_at);
        dateDiff = new DateDiff(date1, date2);
        return dateDiff.days();
      case "1":
        date1 = new Date(issue1.created_at);
        date2 = new Date(issue2.created_at);
        dateDiff = new DateDiff(date2, date1);
        return dateDiff.days();
      case "2":
        date1 = new Date(issue1.updated_at);
        date2 = new Date(issue2.updated_at);
        dateDiff = new DateDiff(date1, date2);
        return dateDiff.days();
      case "3":
        date1 = new Date(issue1.updated_at);
        date2 = new Date(issue2.updated_at);
        dateDiff = new DateDiff(date2, date1);
        return dateDiff.days();
      default:
    }
  }

  getLabelIssues(issues, name) {
    let check = [];
    issues.forEach(issue =>
      issue.labels.forEach(label => {
        console.log(name);
        if (label.name == name) {
          check.push(issue);
        }
      })
    );

    return check;
  }

  getAuthorIssues(issues, name) {
    return issues.filter(issue => {
      return issue.user.login === name;
    });
  }

  getOpenOrCloseIssues(issues, name) {
    return issues.filter(issue => {
      return issue.state === name;
    });
  }

  setPages(leftPage, rightPage) {
    this.setState({
      leftPage,
      rightPage
    });
  }

  filterListItems = (issues, type, name, filterBy) => {
    const {
      seedIssues,
      authorIssues,
      labelIssues,
      stateIssues,
      authorFlag,
      labelFlag,
      stateFlag
    } = this.state;
    let filteredIssues;

    console.log(authorIssues);
    console.log(labelIssues);
    console.log(stateIssues);

    switch (type) {
      case "Authors":
        filteredIssues = this.getAuthorIssues(authorIssues, name);

        let isArrEqual = authorIssues.every(function(element, index) {
          return element === seedIssues[index];
        });
        if (isArrEqual) {
          console.log("here");

          this.setState(
            {
              authorFlag: 0,
              labelFlag: 0,
              stateFlag: 0
            },
            () => {
              this.setState({
                labelIssues: filteredIssues,
                stateIssues: filteredIssues
              });
            }
          );
        }

        if (labelFlag === 0)
          this.setState({
            labelIssues: filteredIssues
          });

        if (stateFlag === 0)
          this.setState({
            stateIssues: filteredIssues
          });

        this.setState({
          authorFlag: 1
        });

        break;
      case "Labels":
        filteredIssues = this.getLabelIssues(labelIssues, name);
        isArrEqual = labelIssues.every(function(element, index) {
          return element === seedIssues[index];
        });
        if (isArrEqual) {
          this.setState(
            {
              authorFlag: 0,
              labelFlag: 0,
              stateFlag: 0
            },
            () => {
              this.setState({
                authorIssues: filteredIssues,
                stateIssues: filteredIssues
              });
            }
          );
        }
        if (authorFlag === 0)
          this.setState({
            authorIssues: filteredIssues
          });

        if (stateFlag === 0)
          this.setState({
            stateIssues: filteredIssues
          });

        this.setState({
          labelFlag: 1
        });

        break;
      case "Issues":
        filteredIssues = this.getOpenOrCloseIssues(stateIssues, name);

        isArrEqual = stateIssues.every(function(element, index) {
          return element === seedIssues[index];
        });

        if (isArrEqual) {
          this.setState(
            {
              authorFlag: 0,
              labelFlag: 0,
              stateFlag: 0
            },
            () => {
              this.setState({
                authorIssues: filteredIssues,
                labelIssues: filteredIssues
              });
            }
          );
        }

        if (authorFlag === 0)
          this.setState({
            authorIssues: filteredIssues
          });

        if (labelFlag === 0)
          this.setState({
            labelIssues: filteredIssues
          });
        this.setState({
          stateFlag: 1
        });

        break;
    }

    this.setState({
      newIssues: filteredIssues
    });
  };

  handlePageChange = async pageType => {
    console.log(pageType);
    console.log(this.state.activePage);
    let activePage = this.state.activePage;
    if (pageType === "Next") {
      activePage++;
    } else {
      activePage--;
    }
    if (activePage <= 0) {
      activePage = 1;
    }
    this.getGithubIssues(activePage);
    this.setState({
      activePage: activePage
    });
    console.log(this.state.activePage);
  };

  myCallback = search => {
    this.setState({
      newIssues: this.state.currentIssues.filter(issue => {
        return issue.title.indexOf(search) !== -1;
      })
    });
  };
  render() {
    return (
      <div className="container">
        <Issues
          issues={this.state.newIssues}
          authors={this.getAuthors()}
          avatars={this.getAvatars()}
          labels={this.getLabels()}
          sortBy={this.sortListItems}
          filterBy={this.filterListItems}
          callbackFromParent={this.myCallback}
          handlePageChange={this.handlePageChange}
        />
      </div>
    );
  }
}

export default App;
